var zlib = require("zlib");
var AWS = require("aws-sdk");

exports.handler = function (input, context) {
  let topic = process.env.TOPIC_ARN;
  var sns = new AWS.SNS();
  var payload = Buffer.from(input.awslogs.data, "base64");

  zlib.gunzip(payload, function (e, result) {
    if (e) {
      context.fail(e);
    } else {
      result = JSON.parse(result.toString());
      console.log("Event Data:", JSON.stringify(result, null, 2));
      console.log("Sending to SNS Topic: ", topic);
      var params = {
        Message: JSON.stringify(result, null, 2),
        Subject: "Detected Root Usage",
        TopicArn: topic,
      };
      var publishTextPromise = new AWS.SNS({ apiVersion: "2010-03-31" })
        .publish(params)
        .promise();
      // Handle promise's fulfilled/rejected states
      publishTextPromise
        .then(function (data) {
          console.log(
            `Message ${params.Message} sent to the topic ${params.TopicArn}`
          );
        })
        .catch(function (err) {
          console.error(err, err.stack);
        });
      sns.publish(params, context.done);
      context.succeed();
    }
  });
};
